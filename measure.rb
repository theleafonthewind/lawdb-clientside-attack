GC.disable

require 'socket'
require 'pp'
require 'hashie'

class Array
	def median
		sorted = self.sort
		elements = sorted.count
		center = elements / 2
		elements.even? ? (sorted[center] + sorted[center+1])/2 : sorted[center]  
	end

	def mean
		self.inject(0, :+).to_f / self.length.to_f
	end

	def deviation
		mid = self.mean
		self.map { |x| x - mid }
	end

	def variance
		self.deviation.map { |x| x*x }.reduce(:+) / (self.length - 1)
	end

	def stdev
		Math.sqrt self.variance
	end

	def to_i
		return (self[0] << 24) | (self[1] << 16) | (self[2] << 8) | (self[3])
	end

	def in_first_dev
		std = self.stdev 
                m = self.mean
                return self.select { |x| ((m - std)..(m + std)).include? x }
	end
end

class LawdbClient
	class << self
		@@cost = 0
	end

        def initialize
                @sock = UDPSocket.new
        end

        def oracle index
                @@cost = @@cost + 1
                return index == 25437474
        end

	def insert key
		@@cost = @@cost + 1
		msg = ([ 1 ] + [ key ].pack('N').unpack('c*'))
		@sock.send msg.pack('C*'), 0, 'localhost', '7689'
		self.receive
	end

        def search min, max
                @@cost = @@cost + 1
                msg = ([ 8 ] + [ min ].pack('N').unpack('c*') + [ max ].pack('N').unpack('c*'))
                @sock.send msg.pack('C*'), 0, 'localhost', '7689'
        end

        def receive
                @sock.recvfrom(4)[0].bytes
        end

	def self.reset!
		@@cost = 0
	end

        def self.cost
                @@cost
        end
end

class Measurement
	attr_accessor :deltas, :total

	def initialize first_mod=1, min=0
		@real_mod = first_mod
		@real_min = min
		@deltas = {}
		@previous = min
		@time = @first_time = Time.now
		@total = []
		@timestamp = @time
	end

	def check new_value
		@timestamp = Time.now
		if new_value > @previous
			key = (@previous..new_value)
			@deltas[key] = [] unless @deltas.has_key? key
			@deltas[key] << ((@timestamp - @time) / @mod)
			@previous = new_value
			@time = Time.now
			@mod = 1
		end
	end

	def reset
		@mod = @real_mod
		@previous = @real_min
	end

	def finish
		@total << Time.now - @timestamp
	end

	def median
		med = Measurement.new
		@deltas.each do |k, v|
			med.deltas[k] = v.median
		end
		return med
	end

	def intervals
		@deltas
	end

	def sorted_intervals
		@deltas.sort_by do |k, v| v end
	end

	def total
		@total.median
	end
end

class Meter
	attr_accessor :current, :config
	
	def initialize config_hash = eval(File.read('config.rb'))
		@cli = LawdbClient.new
		@config = Hashie::Mash.new(config_hash)
	end

	def measure min, max, e = [ 255, 255, 255, 248 ]
		@current = Measurement.new @config[:error_correction][:first_mod], min
		@config[:sample_size][:primary].times do
			@cli.search min, max
			@current.reset
			loop do
				msg = @cli.receive
				break if msg == e
				@current.check msg.to_i
			end
			@current.finish
		end
		self
	end

	def measure_range range, e = [ 255, 255, 255, 248 ]
		measure range.begin, range.end, e
	end

	def measure_default e = [ 255, 255, 255, 248 ]
		measure @config[:min], @config[:max], e
	end
end

