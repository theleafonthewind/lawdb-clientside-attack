# sample size for primary search and binary trace
{
	:min => 10_000,
	:max => 40_000_000,
	:error_correction => {
		:first_mod => 5
	},
	:sample_size => {
		:primary => 10,
		:binary => 10,
		:manual => 10,
	},
	:budget => 500
}
